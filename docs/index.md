# Devenir développeur / développeuse C++
C++ est l’un des langages de programmation les plus populaires au monde. Il est utilisé aussi bien pour la programmation de systèmes d’exploitation que pour la conception d’applications mobiles. Ce langage constitue une base essentielle pour tout programmeur. Devenez un développeur C++ grâce à nos formations réalisées par nos meilleurs experts.

## Objectifs
- Apprenez les principes fondamentaux de la programmation avec C++ : syntaxe, opérateurs, circuits, fonctions, etc.
- Approfondissez vos connaissances en matière de structures de données, d’objets et de templates.
- Créez des environnements de développement, développez des applications et des templates de programmation.

## Liste des Cours
- [ ] Les fondements de la programmation : La conception orientée objet
- [ ] L'essentiel du langage C
- [ ] L'essentiel de C++ 11
- [ ] C++ : L'utilisation des templates
- [ ] C++ : La gestion du multithread
- [ ] C++ : Les pointeurs intelligents
- [ ] C++ : La sémantique de déplacement
- [ ] C++ : Création d'une bibliothèque de chaînes de caractères
- [ ] C++ : ​La gestion des erreurs avec les exceptions
- [ ] L'essentiel de C++ sous Qt 5

## Cours et notebooks
- Les cours sont disponibles via le site du repo, elles sont générés grâce à MKDOCS
- Les notebooks sont disponibles via l'environnement Docker:
    1. Installation, Compilation et Exécution

        ```sudo apt update && sudo apt install -y docker git```
        ```git pull https://gitlab.com/lc-elearning/linkedinlearning/devenir-developpeur-developpeuse-c-plus-plus.git```
        ```cd devenir-developpeur-developpeuse-c-plus-plus```
        ```docker build -t devenir-developpeur-developpeuse-c-plus-plus .```
        ```docker run -ti -p 8888:8888 devenir-developpeur-developpeuse-c-plus-plus```

    2. Ouvrez la page avec le lien indiqué
    3. Les fichiers se trouvent dans le dossier `work/`