## Connaître les bases du langage C++
### Introduction

Pour utiliser de manière performante le C++, il faut penser objet et relation entre objets. Un débutant peut être dérouté par une syntaxe complexe.
```bash
 mingw32-g++ main.cpp -o output.exe
 ```
1. main.cpp => le fichier à compiler
2. liaison du ou des fichiers à compiler
3. création du fichier exécutable output.exe


```c++
#include <iostream>
void helloWorld() {
    char c='c';
    std::cout << "hello world";
}
helloWorld();
```

    hello world

### Utiliser les variables et leurs types
 - nom de variable explicitent
 - toutes les variables doivent être initialisées à la création
 - la portée d'une variable est limitée à son scope de définition
 - la déclaration se fait par convention en début de scope
 - `auto` permet de faire de l'infférence de type
 - `decltype()` permet de déclarer un type de variable à partir du type d'une autre variable 

**Types de varibles:**

| Type | Signification | Exemple |
|----|----|----|
| char | Caractère simple | `char c='c';` |
| wchar | Caractère large | `wchar_t wc='c';` |
| short | Entier Court | `short s=4;` |
| int | Entier Simple | `int i=1;` |
| long | Entier long | `long l=3;` |
| long long | Entier très long | `long long ll=46843;` |
| float | Décimal | `float f=3.14;` |
| double | Décimal faisant le double de float | `double d=3.14;` |
| long double | Décimal très long | `long double ld=3.14;` |
| bool | Booléen compatible numérique | `bool b= true;` `bool b= 1;` |

- L'instruction `sizeof()` permet de connaitre la taille en octet d'un type.
- Les types non-signés `unisgned` font la même taille que les types signés.

- les variables globales existent mais elles sont à proscrire
- les variables sont limités par le scope
- les variables locales redéfinies cachent une définition d'un scope englobant

On peut manipuler une variable avec:
- son pointeur ou addresse mémoire:
    ```c++
        int a = 0;
        int* p_a = &a;
        *p_a = 1; //a=1
    ```
    - le * permet de modifier le type afin de stocker un pointeur
    - le & permet de récupérer l'addresse mémoire d'une variable
- une référence sur la variable:
    ```c++
        int a = 0;
        int& r_a = a;
        r_a = 1; //a=1
    ```
    - une référence sur variable est **obligatoirement** initialisée

Une constante ne permet pas la modification:
```c++
int const cst = 0; //Déclaration d'une constante
int * const ptr = &cst; //Le pointeur est constant
const int * ptr2 = &cst; //Pointeur sur une constante
const int& ref = cst; //Référence sur une constante
```

La référence étant un alias sur la variable, la référence sur données constante est peu utile.

```c++
int tab[5]; //Création d'un tableau de 5 entiers
tab[0] = 0; //Accès à une valeur
for(int i=0; i<5; i++) //Initialisation d'un tableau
    tab[i] = 0;
```
- Pointer vers le premier élément du tableau ou vers le tableau est strictement identique.
- Un tableau a une taille fixe
- Il est possible de se déplacer avec un pointeur en se basant sur l'arithmétique de pointeur:


```c++
#include <iostream>
int tab[5];
for(int i=0; i<5; i++)
    tab[i] = 0;
int * p_tab = &tab[0];
std::cout << "tab[2]="<<tab[2]<<std::endl;
tab[2]=1;
std::cout << "tab[2]="<<tab[2]<<std::endl;
*(p_tab+2)=2;
std::cout << "tab[2]="<<tab[2]<<std::endl;
```

    tab[2]=0
    tab[2]=1
    tab[2]=2


On peut également utiliser la classe `std::array`:


```c++
#include <iostream>
#include <array>
std::array<int, 5> tab;
for(int item : tab)
    std::cout << item << std::endl;
```

    0
    0
    0
    0
    0


Une structure permet de définir un nouveau type de variable contenant d'autre variables:
```c++
struct month{
    int number;
    int days_in_month;
};
month january;
january.number = 1;
january.days_in_month = 31;
```

Une énumération permet de définir les différentes types de valeurs possible pour un type:
```c++
enum e_month{JANUARY, FEBUARY, MARCH};
struct s_month{
    e_month number;
    int days_in_month;
};
s_month january;
january.number = JANUARY;
january.days_in_month = 31;
```
- Si une autre énumération utilise le même nom d'élément il faut le préfixé du nom de l'enum concerné
- Une énumération fait correspondre les différentes éléments de la liste avec des nombres en commencant par 0, il possible de forcer l'attribution de certaines valeurs de la liste:


```c++
#include <iostream>
{
enum e_month1{JANUARY, FEBUARY, MARCH};
struct s_month1{
    e_month1 number;
    int days_in_month;
};
s_month1 january1;
january1.number = e_month1::JANUARY;
january1.days_in_month = 31;
std::cout << "JANUARY=" << january1.number << std::endl;
}

enum e_month2{JANUARY=1, FEBUARY=2, MARCH};
struct s_month2{
    e_month2 number;
    int days_in_month;
};
s_month2 january2;
january2.number = e_month2::JANUARY;
january2.days_in_month = 31;
std::cout << "JANUARY=" << january2.number << std::endl;
```

    JANUARY=0
    JANUARY=1



```c++
#include <iostream>
enum e_month{JANUARY=1, FEBUARY=2, MARCH};
struct s_month{
    e_month number;
    int days_in_month;
};
s_month january;
january.number = JANUARY;
january.days_in_month = 31;
std::cout << "JANUARY=" << january.number << std::endl;
```

    JANUARY=1


Il est possible d'effectuer des conversions d'un type de variable à un autre dans plusiseurs cas:
- la conversion **implicite** : cas 1
- la conversion **forcée/explicite** : cas 2
- la conversion **vérifiée** : cas 3


```c++
#include <iostream>
double d = 3.14;
int i = d;
std::cout << "Cas 1: conversion implicite d'un décimal vers un entier" << std::endl;
std::cout << "Double = "<< d << " Entier = " << i << std::endl;
```

    Cas 1: conversion implicite d'un décimal vers un entier
    Double = 3.14 Entier = 3



```c++
#include <iostream>
int s = -3;
unsigned int u = (unsigned int)s;
std::cout << "Cas 2: conversion forcée/explicite d'un entier signé vers un entier non signé" << std::endl;
std::cout << "Entier Signé = "<< s << " Entier Non-Signé = " << u << std::endl;
```

    Cas 2: conversion forcée/explicite d'un entier signé vers un entier non signé
    Entier Signé = -3 Entier Non-Signé = 4294967293



```c++
#include <iostream>
struct nombre{};
double d = 3.14;
nombre& pi = (nombre&)d;
std::cout << "Cas 3: conversion vérifiée d'un décimal vers une structure" << std::endl;
pi = static_cast<nombre&>(d);
```

    input_line_28:6:6: error: non-const lvalue reference to type '__cling_N512::nombre' cannot bind to a value of unrelated type 'double'
    pi = static_cast<nombre&>(d);
         ^                    ~



    Interpreter Error: 


Il existe plusieurs cycles de vies pour une données:
- le cycle **automatique**, une variable est crée au sein d'une fonction, la mémoire est libérée à la fin de l'exécution de la fonction, il n'est donc pas possible d'utiliser la valeur de la variable via un pointeur par exemple;
- le cycle **dynamique**, la variable est crée sur le tas avec `new`, la mémoire n'est pas libérée après exécution de la fonction mais part l'instruction `delete`;
- le cycle **statique**, une variable globale définie avant le main.

### Contrôler le flux d'exécution

On utilise la syntaxe pour le si...sinon-si...sinon...:
```c++
if(predicat1 && predicat2)
{
    //
}
else if(predicat3 || predicat4)
{
    //
}
else
{
    //
}
```
- on systèmatise l'utilisation du `else`, des `{}` et de l'indentation;
- `nullptr` permet d'initialiser un pointeur à une valuer neutre;
- mettre un pointeur directement dans un `if()` le compare à `nullptr`;
- mettre une attribution dans le `if()` test si l'ttribution c'est bien passée, on cherche donc à mettre la comparaison en premier lorsque l'on écrit un pédicat afin d'éviter les erreurs de syntaxe.
- le C++ fait de l'évaluation paresseuse.

Le `switch` suit la syntaxe suivante:
```c++
switch(var)
{
    case val1:
        //
        break;
    case val2:
    case val3:
        //
        break;
    default:
        //
}
```
- Dans le cas on l'on nécessite la création d'une variable dans un case on l'encadre de `{}` car le compilateur va le confondre avec un `case`;
- On ne peut utiliser seulement des types correpsondant à des `int` (char, int, bool).

La boucle `while` suit la syntaxe suiante:
```c++
while(predicat)
{
    //
}
```

La boucle `do...while` suit la syntaxe suiante:
```c++
do
{
    //
}while(predicat);
```

La boucle `for` suit la syntaxe suiante:
```c++
for(type var = val; predicat; opération_de_boucle)
{
    //
}
```

### Entrer dans les fonctions du C++

La définition de fonction suit la syntaxe suivante:
```c++
type_de_retour nom_de_fonction(type_param nom_param); //Déclaration de la focntion
//
type_de_retour nom_de_fonction(type_param nom_param) //Définition de la fonction
{
    //Traitement
    return val;
}
//
nom_de_fonction(param); // Appel de la fonction
```
- Si une fonction ne retourne pas de valeur (pas de return dans la définition) son type est `void`
- La déclaration s'appelle prototype, il n'est pas obligé de définir des noms de paramèters, seulement les types

- Par défaut, les paramètres sont passés à la fonction par copie.
- On peut passer les paramètres par référence en ajoutant `&` aux types dans le prototype et la définition de la fonction
- On limite au maximum le passage de paramètres par pointeurs

- En C++, on ne copie pas d'objet, autant que possible on effectue des passages par référence
- Le passage par référence coute peu en mémoire VS le passage par copie
- On retourne des valeurs locales par copie, on ne retourne pas de référence car elles seront détruite à la fin d'exécution
- Pour garder l'optimisation du passage par référence mais en bloquant la modification afin de sécuriser le code, on utilise `const` sur les références en paramètres

- La directive pré-processeur `#define` permet de remplacer un élément par une valeur avant la compilation, c'est utile pour des valeurs constantes
- La directive pré-processeur `#include` permet d'inclure des fichiers
- L'ensemble de directives `#ifdef ... #endif` permet d'effectuer des conditions avant la compilation (choix des includes en fct de l'OS, inclusion d'un fichier seulement une fois, excution de code si mode DEBUG/PROD, ...)
- une fonction `inline type fct(var){}` permet de remplacer l'appel d'une fonction par la fonction elle-même si celle-ci est courte, optimisation pour éviter, déclaration + définition + exécution de fct

### Comprendre comment s'organise le code

- On ajoute tous les prototypes dans un **header**, `<object>.h`
- On ajoute toutes les définitions dans une **source**, `<object>.cpp`, correspondant à un header
- On **inclut** les fichiers headers dans la/les source(s) où sont appelées les fonctions:
    ```c++
        #include <iostream>
        #include "<object>.h"
        int main()
        {
            object_fun();
            return 0;
        }
    ```
    - les includes entre `""` incluent des fichiers utilisateurs avec un chemin relatif
    - les includes entre `<>` incluent des fichiers système
- On n'inclu pas de fichier `.cpp`

Pour éviter les redéfinitions d'objets et des inclusions de header infinies, on test si le fichier a déjà été inclu:
```c++
    //Header
    #ifndef __OBJECT__
    #define __OBJECT__
    //prototypes
    #endif
```

- `extern` permet de définir un prototype pour une définition se trouvant dans un `.cpp` mais sans .h
- `extern` marche de manière identique avec les variables
- `extern "C"` permet d'inclure des fonctions venant de fichiers .c et qui sont codées en C

- un **namespace** permet de regrouper des fonctions, objets, variables dans un ensemble et de les isoler
- `<namespace>::fun()` permet d'appeler une fonction contenue dans le namespace
- `using namespace <namespace>;` permet d'éviter de réécrirer le nom de l'espace de nommage à chaque fois
